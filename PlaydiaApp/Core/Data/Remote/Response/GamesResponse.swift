//
//  GamesResponse.swift
//  PlaydiaApp
//
//  Created by Muhamad Irvan on 11/12/22.
//

import Foundation

struct GamesResponse: Codable {
	let results: [GameResponse]
	let count: Int?

	enum CodingKeys: String, CodingKey {
		case results
		case count
	}
}

struct GameResponse: Codable {
	let id: Int?
	let name: String?
	let rating: Double?
	let released: String?
	let backgroundImage: String?

	enum CodingKeys: String, CodingKey {
		case name
		case id
		case released
		case rating
		case backgroundImage = "background_image"
	}
}
