//
//  GameModel.swift
//  PlaydiaApp
//
//  Created by Muhamad Irvan on 11/12/22.
//

import Foundation

struct GameModel: Equatable, Identifiable {
	let id: Int
	let name: String
	let rating: Double
	var released: String?
	let backgroundImage: String?
}
