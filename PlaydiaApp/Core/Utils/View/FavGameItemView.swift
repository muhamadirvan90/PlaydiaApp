//
//  FavGameItemView.swift
//  PlaydiaApp
//
//  Created by Muhamad Irvan on 11/12/22.
//

import SwiftUI
import SDWebImage
import SDWebImageSwiftUI

struct FavGameItemView: View {
	var game: DetailModel
	var body: some View {
		HStack {
			WebImage(url: URL(string: game.backgroundImage ?? ""))
				.renderingMode(.original)
				.resizable()
				.indicator(.activity) // Activity Indicator
				.transition(.fade(duration: 0.5))
				.aspectRatio(contentMode: .fit)
				.frame(width: 120, alignment: .trailing)
				.cornerRadius(5)

			VStack(alignment: .leading) {
				Text(game.name)
					.font(.system(size: 18))
					.fontWeight(.medium)
				HStack(alignment: .lastTextBaseline) {
					Text(String(format: "%.2f", game.rating))
						.font(.system(size: 16))
						.fontWeight(.regular)
					Image(systemName: "star.fill")
						.foregroundColor(.orange)
				}

			}.padding(.leading)
			Spacer()
		}.frame(minWidth: 0, maxWidth: .infinity, alignment: .leading).padding(.horizontal)

	}
}
