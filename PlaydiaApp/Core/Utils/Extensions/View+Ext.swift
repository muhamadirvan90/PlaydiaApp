//
//  ViewErase+Ext.swift
//  PlaydiaApp
//
//  Created by Muhamad Irvan on 11/12/22.
//

import SwiftUI

extension View {
	func eraseToAnyView() -> AnyView {
		AnyView(self)
	}
}
