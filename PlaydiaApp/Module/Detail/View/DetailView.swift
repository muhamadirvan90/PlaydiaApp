//
//  DetailView.swift
//  PlaydiaApp
//
//  Created by Muhamad Irvan on 11/12/22.
//

import SwiftUI
import AVKit
import SDWebImageSwiftUI
import AlertToast

struct DetailView: View {
	@ObservedObject var presenter: DetailPresenter
	@State private var showToast = false
	var body: some View {
		ScrollView {
			if presenter.loadingState {
				HStack {
					Spacer()
					Spinner(isAnimating: true, style: .large).eraseToAnyView()
					Spacer()
				}.padding(.top, 100)
			} else {
				LazyVStack {
					Text(presenter.detail?.name ?? "Title").font(.headline)
					WebImage(url: URL(string: presenter.detail?.backgroundImage ?? ""))
						.resizable()
						.scaledToFill()
						.frame(width: UIScreen.screenWidth, height: 250)
						.clipped()
						.edgesIgnoringSafeArea(.horizontal)
						.overlay(ImageOverlayView(txt: "\(presenter.detail?.released?.convertToDate() ?? "No data released")",
																			icon: (Image(systemName: "calendar.badge.plus"))), alignment: .bottomTrailing)
						.overlay(
							HStack(alignment: .top) {
								VStack(alignment: .leading) {
									Button(action: {
										if !self.presenter.loadingState {
											if self.presenter.isFav {
												self.presenter.deleteFavorite()
											} else {
												self.presenter.addToFavorite()
											}
										}
									}, label: {
										Image(systemName: self.presenter.isFav ? "heart.fill" : "heart")
											.resizable()
											.frame(width: 20, height: 20)
											.foregroundColor(.red)
									})
								}
							}.padding(EdgeInsets(top: 10, leading: 10, bottom: 10, trailing: 10 ))
							, alignment: .topTrailing)
						.highPriorityGesture(
							TapGesture(count: 2)
								.onEnded { _ in
									if self.presenter.isFav {
										self.presenter.deleteFavorite()
										showToast.toggle()
									} else {
										self.presenter.addToFavorite()
										showToast.toggle()
									}
								}
						)
					HStack(alignment: .top) {
						VStack(alignment: .leading) {
							Text("Rating")
								.fontWeight(.bold)
								.foregroundColor(.gray)
							StarsView(rating: Float(presenter.detail?.rating ?? 0))
								.frame(minWidth: 0, maxWidth: .infinity, alignment: .top)
							HStack(alignment: .top) {
								Text(String(format: "%.2f / 5", presenter.detail?.rating ?? 0))
									.multilineTextAlignment(.center)
							}.frame(minWidth: 0, maxWidth: .infinity, alignment: .top)
						}
						.frame(minWidth: 0, maxWidth: .infinity, alignment: .leading)
						Divider()
					}
					.padding(EdgeInsets(top: 0, leading: 10, bottom: 10, trailing: 0))
					Spacer()
					Divider()
					HStack(alignment: .top) {
						VStack(alignment: .leading) {
							Text("Platform")
								.fontWeight(.bold)
								.foregroundColor(.gray)
							HStack(alignment: .top) {
								ForEach(presenter.detail!.parentPlatforms.indices) { index in
									PlatformLogoView(slug: presenter.detail!.parentPlatforms[index].platform.slug )
										.padding(EdgeInsets(top: 1, leading: 2, bottom: 2, trailing: 1))
								}
							}
						}
						.frame(minWidth: 0, maxWidth: .infinity, alignment: .leading)
					}
					.padding(EdgeInsets(top: 0, leading: 10, bottom: 10, trailing: 0))
					Spacer()
					Divider()
					Group {
						Text(presenter.detail?.description?.replacingOccurrences(
							of: "<[^>]+>",
							with: "",
							options: .regularExpression,
							range: nil) ?? "")
						.padding(.all)
						Divider()
					}
				}
			}
		}
		.toast(isPresenting: $showToast) {
			AlertToast(type: .complete(Color.green), title: "Success!")
		}
		.onAppear {
			if !self.presenter.loadingState {
				self.presenter.checkIsFavorite(game: self.presenter.detail!)
			}
		}
	}
}
