//
//  SearchInteractor.swift
//  PlaydiaApp
//
//  Created by Muhamad Irvan on 11/12/22.
//

import Foundation
import Combine

protocol SearchUseCase {
	func getSearch(query: String) -> AnyPublisher<[GameModel], Error>
}

class SearchInteractor: SearchUseCase {
	private let repository: GameRepositoryProtocol

	required init(repository: GameRepositoryProtocol) {
		self.repository = repository
	}

	func getSearch(query: String) -> AnyPublisher<[GameModel], Error> {
		return repository.getSearch(query: query)
	}

}
