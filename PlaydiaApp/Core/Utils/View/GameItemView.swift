//
//  GamegameView.swift
//  PlaydiaApp
//
//  Created by Muhamad Irvan on 11/12/22.
//

import Foundation
import SwiftUI
import SDWebImage
import SDWebImageSwiftUI

struct GameItemView: View {
	var game: GameModel

	var body: some View {
		LazyVStack(alignment: .leading, spacing: 8) {
			Text(game.name)
				.font(.headline)
				.frame(width: UIScreen.screenWidth, alignment: .leading)
				.padding(.horizontal)
				.foregroundColor(Color("Text"))
			WebImage(url: URL(string: game.backgroundImage ?? ""))
				.resizable()
				.scaledToFill()
				.frame(width: UIScreen.screenWidth, height: 250)
				.clipped()
				.overlay(ImageOverlayView(
					txt: String(game.rating ), icon: (Image(systemName: "star.fill"))),
								 alignment: .bottomLeading)
				.overlay(ImageOverlayView(txt: "\(game.released?.convertToDate() ?? "No data released")",
																	icon: (Image(systemName: "calendar.badge.plus"))), alignment: .bottomTrailing)
		}
		Divider()
			.listRowInsets(EdgeInsets())
			.listRowSeparator(.hidden)
			.edgesIgnoringSafeArea(.horizontal)
			.listStyle(PlainListStyle())
	}
}
