//
//  FavoriteView.swift
//  PlaydiaApp
//
//  Created by Muhamad Irvan on 11/12/22.
//

import SwiftUI

struct FavoriteView: View {
	@EnvironmentObject private var navigationManager: NavigationManager
	@EnvironmentObject var presenter: FavoritePresenter
	@State private var linkActive = false

	var body: some View {
		NavigationView {
			ZStack {
				ScrollView {
					VStack {
						if !self.presenter.games.isEmpty {
							ForEach(self.presenter.games, id: \.self) { game in
								self.presenter.linkBuilder(for: String(game.id)) {
									FavGameItemView(game: game)
								}
							}
						} else {
							Spacer()
							Text("No favorite game.")
								.bold()
							Text("Add your favorite games!")
							Spacer()
						}
					}
				}
			}
			.navigationBarTitle("Favorite")
			.navigationBarTitleDisplayMode(.inline)
			.onAppear {
				self.presenter.getGames()
			}
		}
		.navigationViewStyle(StackNavigationViewStyle())
		.onChange(of: navigationManager.activeTab) { _ in
			linkActive = false
		}
	}
}
