//
//  SplashView.swift
//  PlaydiaApp
//
//  Created by Muhamad Irvan on 11/12/22.
//

import SwiftUI

struct SplashView: View {
	@State var isActive = false
	var body: some View {
		VStack {
			if self.isActive {
				let homeUseCase = Injection.init().provideHome()
				let searchUseCase = Injection.init().provideSearch()
				let favoriteUseCase = Injection.init().provideFavorite()
				let homePresenter = HomePresenter(homeUseCase: homeUseCase)
				let searchPresenter = SearchPresenter(searchUseCase: searchUseCase)
				let favoritePresenter = FavoritePresenter(favoriteUseCase: favoriteUseCase)
				ContentView()
					.environmentObject(homePresenter)
					.environmentObject(searchPresenter)
					.environmentObject(favoritePresenter)
			} else {
				ZStack {
					Color("SplashColor").ignoresSafeArea()
					Image("SplashImage")
				}
			}
		}
		.onAppear {
			DispatchQueue.main.asyncAfter(deadline: .now() + 2.5) {
				withAnimation {
					self.isActive = true
				}
			}
		}
	}
}

struct SplashView_Previews: PreviewProvider {
	static var previews: some View {
		SplashView()
	}
}
