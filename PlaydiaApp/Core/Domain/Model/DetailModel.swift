//
//  DetailModel.swift
//  PlaydiaApp
//
//  Created by Muhamad Irvan on 11/12/22.
//

import Foundation

struct DetailModel: Equatable, Identifiable, Hashable {
	let id: Int
	let name: String
	let released: String?
	let rating: Double
	let description: String?
	let backgroundImage: String?
	let backgroundImageAdditional: String?
	let parentPlatforms: [PlatformsModel]
	let genres: [GenreModel]

	public func getGenre() -> [String] {
		return genres.map {
			$0.name
		}   }

	public func getPlatform() -> [String] {
		return parentPlatforms.map {
			$0.platform.slug
		}
	}

}

struct PlatformsModel: Equatable, Hashable {
	let platform: PlatformModel
}

struct PlatformModel: Equatable, Hashable {
	let slug: String
}

struct GenreModel: Equatable, Hashable {
	let name: String
}
