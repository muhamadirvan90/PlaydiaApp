//
//  CustomEmptyView.swift
//  PlaydiaApp
//
//  Created by Muhamad Irvan on 11/12/22.
//

import SwiftUI

struct CustomEmptyView: View {
	var image: String
	var title: String
	var body: some View {
		VStack {
			Image(image)
				.resizable()
				.renderingMode(.original)
				.scaledToFit()
				.frame(width: 250)
			Text(title)
				.font(.system(.body, design: .rounded))
		}
	}
}
