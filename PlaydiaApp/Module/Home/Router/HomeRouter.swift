//
//  HomeRouter.swift
//  PlaydiaApp
//
//  Created by Muhamad Irvan on 11/12/22.
//

import SwiftUI

class HomeRouter {

	func makeDetailView(for gameid: String) -> some View {
		let detailUseCase = Injection.init().provideDetail(gameid: gameid)
		let presenter = DetailPresenter(detailUseCase: detailUseCase, gameId: gameid)
		return DetailView(presenter: presenter)
	}

}
