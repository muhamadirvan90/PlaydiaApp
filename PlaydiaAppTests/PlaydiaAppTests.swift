//
//  PlaydiaAppTests.swift
//  PlaydiaAppTests
//
//  Created by Muhamad Irvan on 11/12/22.
//

import XCTest
import Combine

@testable import PlaydiaApp
final class PlaydiaAppTests: XCTestCase {
	static var fakeResponse: GameResponse = GameResponse(id: 1, name: "Dummy Test",
																											 rating: 5.5, released: "2022-12-22", backgroundImage: "test.png")
	private var cancellables: Set<AnyCancellable>!

	override func setUpWithError() throws {
		cancellables = []
	}

	func testRemoteDataSource() throws {
		let dataSource = DataSourceMockup()
		var test: [GameResponse] = []
		dataSource.getAllGames()
			.sink(receiveCompletion: { completion in
				switch completion {
				case .failure:
					break
				case .finished:
					break
				}
			}, receiveValue: { games in
				test = games
			}).store(in: &cancellables)

		XCTAssert(dataSource.verify())
		XCTAssertEqual(test.count, 1)
	}
}

extension PlaydiaAppTests {

	class DataSourceMockup: RemoteDataSourceProtocol {
		var functionWasCalled = false

		func getAllGames() -> AnyPublisher<[GameResponse], Error> {
			functionWasCalled = true
			return Future<[GameResponse], Error> { completion in
				completion(.success([fakeResponse]))
			}.eraseToAnyPublisher()
		}

		func getDetail(gameId: String) -> AnyPublisher<DetailResponse, Error> {
			return Future<DetailResponse, Error> { completion in
				completion(.success(
					DetailResponse(id: 1, name: "Dummy Name", released: "2022-12-25",
												 rating: 4.5, description: "Dummy description", backgroundImage: "dummy.png",
												 backgroundImageAdditional: "dummy.png",
												 parentPlatforms: [Platforms(platform: Platform(slug: "dummy platform"))],
												 genres: [Genre(name: "dummy genre")])
				))
			}.eraseToAnyPublisher()

		}

		func getSearch(query: String) -> AnyPublisher<[GameResponse], Error> {
			return Future<[GameResponse], Error> { completion in
				completion(.success([fakeResponse]))
			}.eraseToAnyPublisher()

		}

		func verify() -> Bool {
			return functionWasCalled
		}
	}
}
