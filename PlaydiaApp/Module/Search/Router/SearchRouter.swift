//
//  SearchRouter.swift
//  PlaydiaApp
//
//  Created by Muhamad Irvan on 11/12/22.
//

import Foundation
import SwiftUI

class SearchRouter {
	func makeDetailView(for gameId: String) -> some View {
		let detailUseCase = Injection.init().provideDetail(gameid: gameId)
		let presenter = DetailPresenter(detailUseCase: detailUseCase, gameId: gameId)
		return DetailView(presenter: presenter)
	}

}
