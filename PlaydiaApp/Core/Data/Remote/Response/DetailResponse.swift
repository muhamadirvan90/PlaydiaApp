//
//  DetailResponse.swift
//  PlaydiaApp
//
//  Created by Muhamad Irvan on 11/12/22.
//

import Foundation

struct DetailResponse: Codable {
	let id: Int?
	let name: String?
	let released: String?
	let rating: Double?
	let description: String?
	let backgroundImage: String?
	let backgroundImageAdditional: String?
	let parentPlatforms: [Platforms]
	let genres: [Genre]

	enum CodingKeys: String, CodingKey {
		case name
		case id
		case released
		case rating
		case description
		case backgroundImage = "background_image"
		case backgroundImageAdditional = "background_image_additional"
		case parentPlatforms = "parent_platforms"
		case genres = "genres"
	}
}

struct Platforms: Codable {
	let platform: Platform

	enum CodingKeys: String, CodingKey {
		case platform
	}
}

struct Platform: Codable {
	let slug: String?

	enum CodingKeys: String, CodingKey {
		case slug
	}
}

struct Genre: Codable {
	let name: String?

	enum CodingKeys: String, CodingKey {
		case name
	}
}
