//
//  HomeView.swift
//  PlaydiaApp
//
//  Created by Muhamad Irvan on 11/12/22.
//

import SwiftUI
import SDWebImage
import SDWebImageSwiftUI

struct HomeView: View {
	@EnvironmentObject private var navigationManager: NavigationManager
	@EnvironmentObject var presenter: HomePresenter
	@State var index = 0
	@State private var isActive = false
	@State var isLinkProfileActive = false
	var body: some View {
		NavigationView {
			if presenter.homeStatus == HomeStatus.loading {
				Spinner(isAnimating: true, style: .large).eraseToAnyView()
			} else if presenter.homeStatus == HomeStatus.success {
				ScrollView {
					VStack(alignment: .center) {
						ForEach(self.presenter.games, id: \.id) { game in
							self.presenter.linkBuilder(for: String(game.id)) {
								GameItemView(game: game)
							}
						}
						.frame( maxWidth: .infinity)
						.edgesIgnoringSafeArea(.horizontal)
						.listStyle(PlainListStyle())
						.navigationBarTitle(Text("Playdia"), displayMode: .inline)
						.navigationBarItems(
							trailing:
								NavigationLink(destination: ProfileView(), isActive: $isLinkProfileActive) {
									Button(action: {
										self.isLinkProfileActive = true
									}, label: {
										Image("ProfileIcon")
											.resizable()
											.renderingMode(.original)
											.scaledToFit()
											.frame(width: 40, height: 40)
									})
								}
						)
					}
					.navigationViewStyle(StackNavigationViewStyle())
				}
			} else if presenter.homeStatus == HomeStatus.error {
				Text(presenter.errorMessage)
			}
		}
		.onAppear {
			self.presenter.getAllGames()
		}
		.onChange(of: navigationManager.activeTab) { _ in
			isActive = false
		}
	}
}
