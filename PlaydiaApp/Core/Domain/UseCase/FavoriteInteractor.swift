//
//  FavoriteInteractor.swift
//  PlaydiaApp
//
//  Created by Muhamad Irvan on 11/12/22.
//

import Foundation
import Combine

protocol FavoriteUseCase {
	func getListFav() -> AnyPublisher<[DetailModel], Error>
}

class FavoriteInteractor: FavoriteUseCase {
	private let repository: GameRepositoryProtocol

	required init(repository: GameRepositoryProtocol) {
		self.repository = repository
	}

	func getListFav() -> AnyPublisher<[DetailModel], Error> {
		return repository.getListFav()
	}

}
