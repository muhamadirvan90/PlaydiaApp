//
//  ContentView.swift
//  PlaydiaApp
//
//  Created by Muhamad Irvan on 11/12/22.
//

import SwiftUI
class NavigationManager: ObservableObject {
	@Published var activeTab = 0
}
struct ContentView: View {
	@EnvironmentObject var homePresenter: HomePresenter
	@EnvironmentObject var favoritePresenter: FavoritePresenter
	@EnvironmentObject var searchPresenter: SearchPresenter
	@StateObject private var navigationManager = NavigationManager()
	var body: some View {
		TabView {
			HomeView()
				.tabItem {
					Image(systemName: "house.fill")
					Text("Home")
				}
				.tag(0)
			SearchView()
				.tabItem {
					Image(systemName: "magnifyingglass")
					Text("Search")
				}
				.tag(1)
			FavoriteView()
				.tabItem {
					Image(systemName: "heart.fill")
					Text("Favorite")
				}
				.tag(1)
		}
		.environmentObject(navigationManager)
		.accentColor(Color("Text"))
	}
}
