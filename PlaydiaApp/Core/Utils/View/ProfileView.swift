//
//  ProfileView.swift
//  PlaydiaApp
//
//  Created by Muhamad Irvan on 24/12/22.
//

import SwiftUI

struct ProfileView: View {
	var data = Profile()
	var body: some View {
		List {
			Image(data.image)
				.resizable()
				.aspectRatio(contentMode: .fill)
				.clipShape(Circle())
			Text(data.name)
			Text(data.email)
		}
		.navigationBarTitleDisplayMode(.inline)
	}
}
