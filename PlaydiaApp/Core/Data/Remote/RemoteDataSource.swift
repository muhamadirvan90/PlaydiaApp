//
//  RemoteDataSource.swift
//  PlaydiaApp
//
//  Created by Muhamad Irvan on 11/12/22.
//

import Foundation
import Alamofire
import Combine

protocol RemoteDataSourceProtocol: AnyObject {
	func getAllGames() -> AnyPublisher<[GameResponse], Error>
	func getDetail(gameId: String) -> AnyPublisher<DetailResponse, Error>
	func getSearch(query: String) -> AnyPublisher<[GameResponse], Error>
}

final class RemoteDataSource: NSObject {

	private override init() { }

	static let sharedInstance: RemoteDataSource =  RemoteDataSource()

}

extension RemoteDataSource: RemoteDataSourceProtocol {

	func getSearch(query: String) -> AnyPublisher<[GameResponse], Error> {
		return Future<[GameResponse], Error> { completion in
			if !API.key.isEmpty {
				var url = URLComponents(string: Endpoints.Gets.search.url)!
				url.queryItems = [
					URLQueryItem(name: "key", value: API.key)
				]
				if !query.isEmpty {
					url.queryItems?.append(URLQueryItem(name: "search", value: query))
				}
				AF.request(url)
					.validate()
					.responseDecodable(of: GamesResponse.self) { response in
						switch response.result {
						case .success(let value):
							completion(.success(value.results))
						case .failure:
							completion(.failure(URLError.invalidResponse))
						}
					}
			} else {
				print("Doesn't Have API KEY")
			}

		}.eraseToAnyPublisher()
	}

	func getDetail(gameId: String) -> AnyPublisher<DetailResponse, Error> {
		return Future<DetailResponse, Error> { completion in
			if !API.key.isEmpty {
				var url = URLComponents(string: Endpoints.Gets.gameDetail.url + gameId)!
				url.queryItems = [
					URLQueryItem(name: "key", value: API.key)
				]
				AF.request(url)
					.validate()
					.responseDecodable(of: DetailResponse.self) { response in
						switch response.result {
						case .success(let value):
							completion(.success(value))
						case .failure:
							completion(.failure(URLError.invalidResponse))
						}
					}
			} else {
				print("Doesn't Have API KEY")
			}
		}.eraseToAnyPublisher()
	}

	func getAllGames() -> AnyPublisher<[GameResponse], Error> {
		return Future<[GameResponse], Error> { completion in
			if !API.key.isEmpty {
				var url = URLComponents(string: Endpoints.Gets.gamesAll.url)!
				url.queryItems = [
					URLQueryItem(name: "key", value: API.key)
				]
				let parameters: Parameters = [
					"key": API.key
				]
				AF.request(url, parameters: parameters)
					.validate()
					.responseDecodable(of: GamesResponse.self) { response in
						switch response.result {
						case .success(let value):
							completion(.success(value.results))
						case .failure:
							completion(.failure(URLError.invalidResponse))
						}
					}
			} else {
				print("Doesn't Have API KEY")
			}
		}.eraseToAnyPublisher()
	}
}
