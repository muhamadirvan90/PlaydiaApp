//
//  UIScreen+Ext.swift
//  PlaydiaApp
//
//  Created by Muhamad Irvan on 18/12/22.
//

import SwiftUI

extension UIScreen {
	static let screenWidth = UIScreen.main.bounds.size.width
	static let screenHeight = UIScreen.main.bounds.size.height
	static let screenSize = UIScreen.main.bounds.size
}
