//
//  Profile.swift
//  PlaydiaApp
//
//  Created by Muhamad Irvan on 11/12/22.
//

import Foundation

struct Profile: Hashable {
	let name = "Muhamad Irvan Nurodin"
	let email = "muhamadirvan90@gmail.com"
	let image = "PP"
}
