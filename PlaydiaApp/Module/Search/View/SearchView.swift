//
//  SearchView.swift
//  PlaydiaApp
//
//  Created by Muhamad Irvan on 11/12/22.
//

import SwiftUI

struct SearchView: View {
	@EnvironmentObject private var navigationManager: NavigationManager
	@EnvironmentObject var presenter: SearchPresenter
	@State var query: String = ""
	@State private var linkActive = false

	var body: some View {
		NavigationView {
			ZStack {
				ScrollView {
					HStack {
						HStack {
							Image(systemName: "magnifyingglass")
							TextField("Search", text: $query, onCommit: {
								self.presenter.getSearch(query: query)
							})
							if !query.isEmpty {
								Button(action: {
									self.query = ""
									self.presenter.emptyGames()
								}, label: {
									Image(systemName: "xmark.circle.fill")
								})
							}
						}
						.padding()
						.overlay(RoundedRectangle(cornerRadius: 5).stroke(Color("Text"), lineWidth: 1))

					}.padding(.horizontal).padding(.vertical)

					VStack {
						if presenter.loadingState {
							Spinner(isAnimating: true, style: .large).eraseToAnyView()
						} else {
							if !self.presenter.games.isEmpty {
								ForEach(self.presenter.games, id: \.id) { game in
									self.presenter.linkBuilder(for: String(game.id)) {
										GameItemView(game: game)
									}
								}
							} else {
								Spacer()
								Text("No record found.")
									.bold()
								Text("Try another keyword!")
								Spacer()
							}
						}
					}
					.frame( maxWidth: .infinity)
					.edgesIgnoringSafeArea(.horizontal)
					.listStyle(PlainListStyle())
				}
			}
			.navigationBarTitle("Search")
			.navigationBarTitleDisplayMode(.inline)
		}
		.onChange(of: navigationManager.activeTab) { _ in
			linkActive = false
		}
	}
}
