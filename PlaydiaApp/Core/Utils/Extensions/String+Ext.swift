//
//  String+Ext.swift
//  PlaydiaApp
//
//  Created by Muhamad Irvan on 18/12/22.
//

import SwiftUI

extension String {
	func convertToDate() -> String {
		let dateFormatter = DateFormatter()
		dateFormatter.dateFormat = "yyyy-MM-dd"
		let date = dateFormatter.date(from: self)
		return date?.convertToString(format: "MMM dd, yyyy") ?? "No Release Date"
	}
}

extension Date {
	func convertToString(format: String) -> String {
		let formatter = DateFormatter()
		formatter.dateFormat = format
		return formatter.string(from: self)
	}
}
