//
//  APICall.swift
//  PlaydiaApp
//
//  Created by Muhamad Irvan on 11/12/22.
//

import Foundation

private func getConfig(key: String) -> String {
	var nsDictionary: NSDictionary?
	if let path = Bundle.main.path(forResource: "rawg", ofType: "plist") {
		nsDictionary = NSDictionary(contentsOfFile: path)
	}

	return nsDictionary![key]! as? String ?? ""
}

struct API {
	static let baseUrl = getConfig(key: "apiUrl")
	static let key = getConfig(key: "apiKey")
}

protocol Endpoint {
	var url: String { get }
}

enum Endpoints {
	enum Gets: Endpoint {
		case gamesAll
		case gameDetail
		case search

		public var url: String {
			switch self {
			case .gamesAll: return "\(API.baseUrl)"
			case .gameDetail: return "\(API.baseUrl)/"
			case .search: return "\(API.baseUrl)"
			}
		}
	}

}
