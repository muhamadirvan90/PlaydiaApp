//
//  HomeInteractor.swift
//  PlaydiaApp
//
//  Created by Muhamad Irvan on 11/12/22.
//

import Foundation
import Combine

protocol HomeUseCase {
	func getAllGames() -> AnyPublisher<[GameModel], Error>
}

class HomeInteractor: HomeUseCase {
	private let repository: GameRepositoryProtocol

	required init(repository: GameRepositoryProtocol) {
		self.repository = repository
	}

	func getAllGames() -> AnyPublisher<[GameModel], Error> {
		return repository.getAllGames()
	}
}
