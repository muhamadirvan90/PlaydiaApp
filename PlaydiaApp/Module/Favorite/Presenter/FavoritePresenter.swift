//
//  FavoritePresenter.swift
//  PlaydiaApp
//
//  Created by Muhamad Irvan on 11/12/22.
//

import SwiftUI
import Combine
import Foundation

class FavoritePresenter: ObservableObject {
	private var router = FavoriteRouter()
	private var cancellables: Set<AnyCancellable> = []
	private let favoriteUsecase: FavoriteUseCase

	@Published var games: [DetailModel] = []
	@Published var errorMessage: String = ""

	init(favoriteUseCase: FavoriteUseCase) {
		self.favoriteUsecase = favoriteUseCase
	}

	func getGames() {
		self.favoriteUsecase.getListFav()
			.receive(on: RunLoop.main)
			.sink(receiveCompletion: { (error) in
				self.errorMessage = String(describing: error)
			}, receiveValue: { value in
				self.games = value
			})
			.store(in: &cancellables)
	}

	func linkBuilder<Content: View>(
		for gameId: String,
		@ViewBuilder content: () -> Content
	) -> some View {
		NavigationLink(
			destination: router.makeDetailView(for: gameId)) {content()}
	}
}
